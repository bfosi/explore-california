# System Dependencies

- [kind](https://kind.sigs.k8s.io/docs/user/quick-start/)
- [direnv](https://direnv.net/docs/installation.html)
- [kubectl](https://kubernetes.io/docs/tasks/tools/)

## Setup

```sh
kind create cluster --config kind.yml

# substitute your GitLab username; provide a personal access token for the password
kubectl create secret docker-registry gitlab --docker-server registry.gitlab.com --docker-username <username> --docker-password <password>

kubectl apply -f deployment.yml
```
